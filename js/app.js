var camera, scene, renderer;

var width = window.innerWidth, // 720,
    height = window.innerHeight; // 576;

var camera_angle = 45, // угол обзора камеры
    screen_aspect = width / height, // соотношение сторон экрана
    near = 0.1, // минимальная видимость
    far = 10000; // минимальная видимость
    camera_position_z = 1000; // отдаленность камеры от центра осей по оси z
    renderer_bg_color = 0xEEEEEE;

var parallelepiped, sphere, triangle; // models
var cubeItems = new Array();

var $container = $('#container');

var gui = new dat.GUI();

init();
stats();
createPointLight(250, 50, 1000);
//createParallelepiped();
create3DMenu();
//createSphere();
//createTriangle();
render();

function init() {
    camera = new THREE.PerspectiveCamera( camera_angle, screen_aspect, near, far );
    camera.position.z = camera_position_z;

    scene = new THREE.Scene();

    renderer = new THREE.WebGLRenderer(); //CanvasRenderer
    renderer.setSize( width, height );
    renderer.setClearColor( renderer_bg_color );
    renderer.clear();

    $container.append( renderer.domElement );
}

function create3DMenu() {
    var element_height = 50;
    var elements_count = 11;

    for (var i = 0; i < elements_count; i++) {
        createCubeItem(400, (elements_count-i)*(element_height+5), Math.PI/4, 250, element_height, 250, i);
    }
}

function createCubeItem(positionX, positionY, rotateY, width, height, depth, number) {

    console.log(number);

    var geometry, material, texture;

    geometry = new THREE.BoxGeometry( width, height, depth );

    var canvas = document.createElement('canvas');
    canvas.width = 250;
    canvas.height = height;

    var context = canvas.getContext('2d');

    context.fillStyle = "#136579";
    context.fillRect( 0, 0, 70, height );

    context.fillStyle = "#c6c6c6";
    context.fillRect( 20, 10, 30, 30);

    context.fillStyle = "#1794B2";
    context.fillRect( 70, 0, canvas.width-70, height );


    context.fillStyle = '#ffffff';
    context.font = '24px Arial';
    context.fillText( "Program" + number, 80, 33, 250 );
    texture = new THREE.Texture(canvas);
    texture.needsUpdate = true;

    material = new THREE.MeshLambertMaterial({
        map: texture
    });

    cubeItems[number] = new THREE.Mesh( geometry, material );
    cubeItems[number].rotation.y = rotateY;
    cubeItems[number].position.x = positionX;
    cubeItems[number].position.y = positionY-300;

    scene.add( cubeItems[number] );
}

function createParallelepiped() {
    var geometry, material;

    geometry = new THREE.BoxGeometry( 250, 600, 250 );

//    material = new THREE.MeshLambertMaterial({color: 0x1794B2});

    var canvas = document.createElement('canvas');
    canvas.width = 250;
    canvas.height = 600;
    var element_height = 50;
    var elements_count = 11;
    var context = canvas.getContext('2d');

    for (var i = 0; i < elements_count; i++) {
        context.fillStyle = "#0E4856";
        context.fillRect(0, i*(element_height+5), 70, element_height);

        context.fillStyle = "#1794B2";
        context.fillRect(70, i*(element_height+5), canvas.width-70, element_height);
    }

    context.fillStyle = '#ffffff'; // CHANGED
//    context.textAlign = 'center';
    context.font = '24px Arial';
    context.fillText("some text", 80, 30, 250);
    var amap = new THREE.Texture(canvas);
    amap.needsUpdate = true;

    material = new THREE.MeshLambertMaterial({
        map: amap,
//        color: 0x1794B2
    });

    parallelepiped = new THREE.Mesh( geometry, material );
    parallelepiped.rotation.y = Math.PI / 4;
    parallelepiped.position.x = 400;

    gui.add(parallelepiped.position, 'x').min(-400).max(400).step(10);

    scene.add( parallelepiped );
}

function createSphere() {
    var geometry, texture, material;
    var onLoad, onProgress, onError;
    var textureLoader = new THREE.ImageLoader();

    geometry = new THREE.SphereGeometry(300, 40, 40);
    texture = new THREE.Texture();


    onLoad = function (event) {
        texture.image = event;
        texture.needsUpdate = true;
    };
    onProgress = function (event) {
        console.log(event);
    };
    onError = function (event) {
        console.log(event);
    };
    textureLoader.load('textures/earth_texture.jpg', onLoad, onProgress, onError);

    material = new THREE.MeshBasicMaterial({map: texture});

    sphere = new THREE.Mesh(geometry, material);

    scene.add(sphere);
}

function createTriangle() {
    var geometry, material;

    var vertex1 = new THREE.Vector3(100, -100, 0),
        vertex2 = new THREE.Vector3(0, 100, 0),
        vertex3 = new THREE.Vector3(-100, -100, 0),
        vertex4 = new THREE.Vector3(100, -100, 0);

    geometry = new THREE.Geometry();
    material = new THREE.LineBasicMaterial({color:0x000000});

    geometry.vertices.push(vertex1);
    geometry.vertices.push(vertex2);
    geometry.vertices.push(vertex3);
    geometry.vertices.push(vertex4);

    triangle = new THREE.Line(geometry, material);
    triangle.matrixAutoUpdate = false;

    scene.add(triangle);
}


function createPointLight(x, y, z) {
    var pointLight = new THREE.PointLight(0xFFFFFF);

    pointLight.position.x = x;
    pointLight.position.y = y;
    pointLight.position.z = z;

    scene.add(pointLight);
}

function render() {
    requestAnimationFrame(render);

//    sphere.rotation.y += 0.5*Math.PI/180;

    renderer.render(scene, camera);
}

function leftClick(e) {
    e.preventDefault();

    var i = 0;
    var rotationRender = function() {
        if (i == 30) {
            cancelAnimationFrame(requestId);
            requestId = undefined;
            return false;
        }

        requestId = requestAnimationFrame(rotationRender);
        cubeItems.forEach(function(item) {
            item.rotation.y -= Math.PI / 60;
        });
//        parallelepiped.rotation.y -= Math.PI / 60;
        i++;
    };
    rotationRender();
}

function rightClick(e) {
    e.preventDefault();

    var i = 0;
    var rotationRender = function() {
        if (i == 30) {
            cancelAnimationFrame(requestId);
            requestId = undefined;
            return false;
        }

        requestId = requestAnimationFrame(rotationRender);
        cubeItems.forEach(function(item) {
            item.rotation.y += Math.PI / 60;
        });
//        parallelepiped.rotation.y += Math.PI / 60;

        i++;
    };
    rotationRender();
}

$(document).on('keydown', function(e) {
    var code = e.keyCode || e.which;

    switch (code) {
        case 13: // OK
            break;
        case 27: // ESC
            break;
        case 37: // LEFT
            if (typeof requestId == 'undefined') {
                leftClick(e);
            }
            break;
        case 38: // TOP
            break;
        case 39: // RIGHT
            if (typeof requestId == 'undefined') {
                rightClick(e);
            }
            break;
        case 40: // BOTTOM
            break;
    }
});



//var triangleRender = function () {
//    requestAnimationFrame(triangleRender);

//    var updateMatrix = triangle.matrixWorld;
//    updateMatrix.set( 1, 0, 0, 3,
//                      0, 1 , 0, 0,
//                      0, 0, 1, 10,
//                      0, 0, 0, 1);


//    updateMatrix.setPosition(10, 0, 0);
//    updateMatrix.extractRotation(Math.PI/100);

//    triangle.matrix.multiply(updateMatrix);
//};
//triangleRender();



// Stats
function stats() {
    var stats = new Stats();
    stats.setMode(0); // 0: fps, 1: ms

    stats.domElement.style.position = 'absolute';
    stats.domElement.style.left = '0px';
    stats.domElement.style.top = '0px';

    document.body.appendChild( stats.domElement );

    setInterval( function () {
        stats.begin();

        // your code goes here

        stats.end();
    }, 1000 / 60 );
}



